pipelineJob('MyMaven_pipeline') {

    definition {
        cpsScm {
            scm {
                git {
                    remote {
                            url('git@bitbucket.org:thirump/mymavenapp.git')
                            credentials('42b8cedc-b545-40bc-b23c-619eebdbd4e3')
                    }
                scriptPath('Jenkinsfile')
                branch('*/master')                    
                }
            }
        }
    }
}