pipelineJob('spring_petstore_pipeline') {

    definition {
        cpsScm {
            scm {
                git {
                    remote {
                            url('git@bitbucket.org:thirump/spring-petclinic.git')
                            credentials('42b8cedc-b545-40bc-b23c-619eebdbd4e3')
                    }
                scriptPath('Jenkinsfile')
                branch('*/main')                    
                }
            }
        }
    }
}